# qlik-demo

A demo of a simple REST API to check if messages are palindromes


## API

Please refer to the [swagger documentation](http://qlik.jrahme.ca:5253)

## Architecture

This demo was written in Clojure, using the [Duct](https://github.com/duct-framework/duct) framework, leveraging the [Ataraxy](https://github.com/weavejester/ataraxy) library for routing, and the [carmine](https://github.com/ptaoussanis/carmine) library for data persistance.

Initially the app used the [Compojure](https://github.com/weavejester/compojure) library for routing, but the library was a little outdated, and left some to be desired in terms of a repl based development cycle.

### Libraries Used
 * [Clojure 1.10.0](https://github.com/clojure/clojure)
 * [Duct Core 0.7.0](https://github.com/duct-framework/duct)
 * [Duct Logging Module 0.4.0](https://github.com/duct-framework/module.logging)
 * [Duct Web Module 0.7.0](https://github.com/duct-framework/module.web)
 * [Duct Ataraxy Module 0.3.0](https://github.com/duct-framework/module.ataraxy)
 * [Duct Redis Carmine Database 0.1.1](https://github.com/duct-framework/database.redis.carmine)
 * [Clojure Json Data 0.2.6](https://github.com/clojure/data.json)

### System Overview

#### Persistance

Data is persisted using a Redis database, with all palindromes being stored in a single hashmap with a numeric instance.

The palindromes are stored in the following format;

```json
 {
   "id": 34,
   "msg": "Eva, Can I Stab Bats In A Cave?",
   "palindromes": {
       "alphanumeric": {
           "trimmed": {
               "case-sensitive": false,
               "case-insensitive": true
           },
           "untrimmed": {
               "case-sensitive": false,
               "case-insensitive": true
           }
       },
       "alphanumeric-spaced": {
           "trimmed": {
               "case-sensitive": false,
               "case-insensitive": false
           },
           "untrimmed": {
               "case-sensitive": false,
               "case-insensitive": false
           }
       },
       "all-characters": {
           "trimmed": {
               "case-sensitive": false,
               "case-insensitive": false
           },
           "untrimmed": {
               "case-sensitive": false,
               "case-insensitive": false
           }
       }
   }
 }
```
Because of the loose definition of a palindrome in the requirements document, the app will check for multiple palindrome cases. Following the branches of the returned JSON dictionary to the leaf a user can determine under which conditions the message qualifies as a palindrome.

### Project

The source code for the project can be found in the ./src/qlik-demo/src/qlik_demo directory for the project. 

The code is divided into 3 different namespaces, with the main namespace residing above all three. 

#### boundary

The boundary namespace is used as a part of the Duct framework for describing the protocols and methods required to interface with an external datasource. This allows the application layer code to be kept independent of the data layer code, allowing for more modularity, and an ease in switching datasoruces (if the new boundry record implements the same protocols as the previous boundary)

In this directory there is only one boundary described, in the redis.clj file.

##### qlik-demo.boundary.redis Namespace

This namespace is not very interesting, and is essentially just basic crud operations for interacting with the Redis backend. If it were to be scoured for somethign that was almost interesting it would present the search-message and list-messages functions

The search-message function, gets all the keys and messages as pairs from the redis database, and then searches for pairs that match the passed msg parameter exactly. It then messages the matching key / message pairs into a collection of two vectors, on of the ordered keys and the other of the ordered messages. These are then zipped into a hashmap and returned to the calling function.

The list-messages function also takes all messages stored in teh database, but simply just sorts them into a collection of vectors where the first element is the key and the second element the stored message.

#### impl

The impl namespace is the core application logic. In this case there is only one file, palindrome.clj, which contains the logic for determining if a message is a palindrome, and if so under which conditions it is a palindrome.

##### qlik-demo.impl.plaindrome Namespace

There is only one public function in this namesapce which is called is-palindrome?. 

This function takes a single string and then creats a collection of vectors that describe all of the cases described by the palindrome-types, message-manipulations, and msg-cases collections.

Each of these collections describes a different set of cases that could be considered a palindrome. For each unique combination of the inner vectors of these collections a new vector is rendered that describes a particular palindrome case with the tail element being a boolean value describing if the message is a palindrome under these conditions. This logic is applied in the apply-tuples function. The resulting collection from this function is then taken and reduced into a hashmap where each key describes a condition of the case, with the leaf value being the resulting boolean of if the message is a palindrome for that case.

With more time the apply-tuples function could be refactored into a macro that could take an arbitrary number of keyword / case function tuple collections and generate the function to determine if a message is a palindrome for those cases, as the function follows a very simple pattern of mapping each collection against the other. 

To better visualize the pattern followed for the apply-tuples function, if written in python it would look something like;

```python
def apply_tuples (msg):
  palindrome_cases = []
  for palindrome_type, palindrome_fn in palindrome_types:
    for manipulation_type, manipulation_fn in msg_manipulations:
      for case_type, case_fn in msg_cases:
        palindrome_cases.append([palindrome_type, manipulation_type, case_type, palindrome_fn(manipulation_fn(case_fn(msg)))])
  return palindrome_cases
```

###### Determining If a Message is a Palindrome

A message is determined to be a palindrome if, after the various cases have been applied, by;
 
 * determining the length of the message
 * splitting the message into two even character collections
  * if the message is of an odd length, the second collection will be longer by 1 character which is removed from the head of the second collection. Because this character will always be the same when read forwards or backwards, it can safely be removed from the collections
 * The second collection is then reversed and compared against the first collection.
 * If the two collections are equivillant than the message is determined to be a palindrome for that case.

#### handler

The handler namespace is used for the request handlers to process the RESTful requests, and return a response to the client.

##### qlik-demo.handler.message Namespace

This namespace contains two helper functions for generating a response (->response) and formatting the response message into a standard datastructure (->msg)

There are six RESTful endpoints described in this handler;

 * create [POST /message]
 * get-all [GET /messages]
 * get [GET /message]
 * update [PUT /message]
 * delete [DELETE /message]
 * search [GET /search]

Usage of these endpoints are described in the API Refrence section of this document

## Launching

### Requirements

#### Docker Compose

The docker compose file only requires that docker and docker-compose be installed on the machine to run.

#### All Other Methods

If not using docker compose a Redis Databse needs to be available for the api to function correctly.

##### Install Redis

Redis can be installed by following the [Redis Quick Start Documentation](https://redis.io/topics/quickstart) or via Docker by using the [Redis Dockerfile](https://hub.docker.com/\_/redis)

##### Configure Redis Host For App

By default the app will look for redis under the hostname redis, as that is what the hostname the redis service is presented under in the docker overlay network when launched via docker-compose. To change the redis hostname to something else you can run

```sh
sed -i 's/:host "redis"/:host "YOUR_REDIS_HOST"/' resources/qlik-demo/config.edn
```

### Running the full stack

The entire stack can be deployed by using the included docker-compose file.

The API is made available on port 8080

Documentation is made available on port 5253

```sh
docker-compose up --no-start
docker-compose start
```

### Building the Image

The image can be built via the included Dockerfile with the command

```sh
docker build . -t qlik-demo

```
### Pulling the Image

A prebuilt image can also be pulled down from the gitlab container registry

```sh
docker pull registry.gitlab.com/jrahme/qlik-demo:app
```

### Running the Image

The image can be ran via docker wit the command

```sh
docker run -d --name qlik-demo registry.gitlab.com/jrahme/qlik-demo:app
```

### Running Natively

This app can be ran as a standalone Java process with the following instructions

```sh
cd /path/to/project
lein uberjar
java -jar target/qlik-demo-0.1.0-SNAPSHOT-standalone.jar
```

### Running in a REPL

This app can be ran using a REPL via the [leinigen](https://leiningen.org/) tool. Details on how to do so can be found in the Developing section of this document

## Developing

### Setup

When you first clone this repository, run:

```sh
lein duct setup
```

This will create files for local configuration, and prep your system
for the project.

### Environment

To begin developing, start with a REPL.

```sh
lein repl
```

Then load the development environment.

```clojure
user=> (dev)
:loaded
```

Run `go` to prep and initiate the system.

```clojure
dev=> (go)
:duct.server.http.jetty/starting-server {:port 3000}
:initiated
```

By default this creates a web server at <http://localhost:3000>.

When you make changes to your source files, use `reset` to reload any
modified files and reset the server.

```clojure
dev=> (reset)
:reloading (...)
:resumed
```

### Testing

Testing is fastest through the REPL, as you avoid environment startup
time.

```clojure
dev=> (test)
...
```

But you can also run tests through Leiningen.

```sh
lein test
```

Tests are ran as a part of the CI/CD pipeline, with lein test being ran before building the image, and api testing ran after deployment.

### CI/CD

The CI/CD implementation for this app uses the Gitlab-CI with the jobs for the pipeline described in the .gitlab-ci file in this repo. 

## Legal

Copyright © 2019 Jrahme
