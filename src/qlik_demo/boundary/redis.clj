(ns qlik-demo.boundary.redis
  (:require [duct.database.redis.carmine]
            [taoensso.carmine :as car :refer (wcar)]))

(def message-hashset "qlik:messages")

(defprotocol QlikPalindromes
  (get-message [db id])
  (list-messages [db])
  (set-message [db id msg])
  (search-message [db msg])
  (update-message [db id msg])
  (delete-message [db id])
  (next-index[db])
  (id-exists? [db id])
  (msg-exists? [db msg]))

(extend-protocol QlikPalindromes
  duct.database.redis.carmine.Boundary

  (next-index
    [db]
    (wcar (:conn-opts db) (car/incr "qlik:index")))

  (id-exists?
    [db id]
    (some #{id} (wcar (:conn-opts db)
                      (car/hkeys message-hashset))))

  (msg-exists?
    [db msg]
    (if (empty? (search-message db msg))
      false
      true))

  (get-message
    [db id]
    (if (id-exists? db id)
      (wcar (:conn-opts db)
            (car/hget message-hashset id))
      false))

  (list-messages
    [db]
    ;; parse the redis hashset into a clojure map
    (reduce #(conj %1 (assoc (second %2) :id (first %2))) 
            [] 
            (partition 2 (car/parse-map (wcar (:conn-opts db) (car/hgetall message-hashset)))))) 

  (set-message
    [db id msg]
    (wcar (:conn-opts db)
          (car/hset message-hashset id msg)))

  (update-message
    [db id msg]
    (if (id-exists? db id)
      (wcar (:conn-opts db)
            (car/hset message-hashset id msg))
      false))

  (delete-message
    [db id]
    (if (id-exists? db id)
      (wcar (:conn-opts db)
            (car/hdel message-hashset id))
      false))

  (search-message
    [db msg]
    ;;zip the kv collections into a map
    (apply zipmap
           ;;transform k/v pairs a vector of two collections for keys and values respectively
           (reduce 
             (fn [acc [k v]] [(conj (first acc) k) (conj (second acc) v)])
             [[][]]
             ;; filter for messages that are equal to the passed message
             (filter 
               (fn [[_ rdb-msg]] (= msg (:msg rdb-msg))) 
               (partition 2 (wcar (:conn-opts db) (car/hgetall message-hashset))))))))
