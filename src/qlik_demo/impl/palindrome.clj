(ns qlik-demo.impl.palindrome
  (:require [clojure.string :refer [split join trim trim-newline lower-case]]))

(def ^:private split-msg 
  (memoize 
    (fn split-msg
      [msg]
      (let [len (count msg)
            hlvs (split-at (int (/ len 2)) msg)]
        (if (odd? len)
          (update hlvs 1 rest)
          hlvs)))))

(def ^:private trim-msg (memoize (comp trim trim-newline)))


(defn- palindrome?
  [msg]
  (apply #(= %1 (reverse %2)) (split-msg msg)))

(defn- filtered-palindrome?
  [fltr msg]
  (palindrome? (filter fltr msg)))

;; Filter functions
(defn- alphanumeric-filter 
  [chr]
  (Character/isLetterOrDigit chr))

(defn- alphanumeric-spaces-filter 
  [chr]
  (or (Character/isLetterOrDigit chr) (Character/isSpaceChar chr)))

;; palindrome checking functions
(def ^:private alphanumeric-palindrome? (partial filtered-palindrome? alphanumeric-filter))
(def ^:private alphanumeric-spaced-palindrome? (partial filtered-palindrome? alphanumeric-spaces-filter))

;;; helper for the tuples
(defn- returner [a] a)

;; ideally this would be refractored into a macro that could take an arbitrary amount of tuple collections, instead of only the 3 applied here
;;  but this is fairly flexible, allowing for a 3 by 4 matrix of palindrome possibilites. It can be horizontally expanded by adding more tuples to
;; any of the 3 tuple collections below
(def ^:private palindrome-types [[:alphanumeric alphanumeric-palindrome?] [:alphanumeric-spaced alphanumeric-spaced-palindrome?] [:all-characters palindrome?]])
(def ^:private msg-manipulations [[:trimmed trim-msg] [:untrimmed returner]] ) 
(def ^:private msg-cases [[:case-sensitive returner] [:case-insensitive lower-case]])

(defn- apply-tuples
  [msg]
  (map
    (fn [[palindrome-kw palindrome-func]]
      (map 
        (fn [[manip-kw manip-func]]
          (map
            (fn [[case-kw case-func]]
              [[palindrome-kw manip-kw case-kw] ((comp palindrome-func manip-func case-func) msg)])
            msg-cases))
        msg-manipulations))
    palindrome-types))

(defn is-palindrome?
  [msg]
  (reduce 
    (fn [acc tupe] (assoc-in acc (drop-last tupe) (last tupe))) 
    {} (partition 4 (flatten (apply-tuples (if (empty? msg) " " msg))))))

