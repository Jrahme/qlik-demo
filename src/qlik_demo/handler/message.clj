(ns qlik-demo.handler.message
  (:require [qlik-demo.boundary.redis :as rdb]
            [qlik-demo.impl.palindrome :refer [is-palindrome?]]
            [ataraxy [core :as ataraxy]
             [response :as response]]
            [ring.util.response :as resp]
            [integrant.core :as ig]
            [clojure.data.json :refer [write-str]]))

(defn- ->body
  [msg & {:keys [created deleted modified retrieved]
          :or {created nil deleted nil modified nil retrieved nil}}]
  {:msg msg
   :data (reduce-kv (fn [m k v]
                      (if (nil? v) m (assoc m k v))) {} {:created created :modified modified 
                                                         :deleted deleted :retrieved retrieved})})
(defn- ->msg
  [id msg]
  {:id id
   :msg msg
   :palindromes (is-palindrome? msg)})

(extend-protocol response/ToResponse
  clojure.lang.PersistentArrayMap
  (->response [m]
    (-> (write-str m) resp/response (resp/content-type "application/json"))))

(defmethod ig/init-key :qlik-demo.handler.message/create
  [_ {:keys [db]}]
  (fn [{[_ msg] :ataraxy/params :as request}]
    (let [msg (->> request :params :msg)]
      (if-let [id (rdb/msg-exists? db msg)] 
        [::response/conflict (->body "This message already exists" :retrieved (rdb/search-message db msg))]
        (let [index (rdb/next-index db)
              msg-data (->msg index msg)
              res (rdb/set-message db index msg-data)]
          (println (type (->body (str "Message created with id " index)
                                 :created msg-data)))
          [::response/ok (->body (str "Message created with id " index)
                                 :created msg-data)])))))

(defmethod ig/init-key :qlik-demo.handler.message/get-all
  [_ {:keys [db]}]
  (fn [_]
    (let [msgs (rdb/list-messages db)]
      [::response/ok (->body (str (if (empty? msgs) "No m" "M")"essages found") :retrieved msgs)])))

(defmethod ig/init-key :qlik-demo.handler.message/get
  [_ {:keys [db]}]
  (fn [{[_ form] :ataraxy/result :as request}]
    (let [id (->> request :params :id)]
      (if-let [res (rdb/get-message db id)]
        [::response/ok (->body "message found" :retrieved res)] 
        [::response/not-found (->body (str "No message found for id " id))]))))

(defmethod ig/init-key :qlik-demo.handler.message/update
  [_ {:keys [db]}]
  (fn [{[_ form] :ataraxy/result :as request}]
    (let [id (->> request :params :id)
          new-msg (->msg id (->> request :params :msg))
          old-msg (if (rdb/id-exists? db id) (rdb/get-message db id) false)]
      (if old-msg
        ;; check if the old message is the same as the new message
        (if (= (:msg old-msg) (:msg new-msg))
          [::response/ok (->body "No difference compared to current message; not updating." :retrieved (old-msg))]
          [::response/accepted (->body (str "Message " id " updated") :modified new-msg :retrieved old-msg)])
        ;; error out stating no message of that value to update
        [::response/not-found (->body (str "No message found for id " id))]))))

(defmethod ig/init-key :qlik-demo.handler.message/delete
  [_ {:keys [db]}]
  (fn [{[_ form] :ataraxy/params :as request}]
    (let [id  (->> request :params :id)]
      (if (rdb/id-exists? db id)
        (let [msg (rdb/get-message db id)
              del-res (rdb/delete-message db id)]
          (if (= del-res (or 1 "1"))
            [::response/ok (->body (str "msg " id " successfully deleted") :deleted (:msg msg) :retreived msg)]  
            [::response/internal-server-error (->body (str "Internal error while removing id " id) :retreived msg)]))
        [::response/not-found (->body (str "No message found for id " id))]))))

(defmethod ig/init-key :qlik-demo.handler.message/search
  [_ {:keys [db]}]
  (fn [{[_ form] :ataraxy/result :as request}]
    (let [msg (->> request :params :msg)]
      (if-let [found (seq (filter #(not (empty? %1))(rdb/search-message db msg)))]
        [::response/ok (->body "Message found" :retrieved (->> found first second))]
        [::response/not-found (->body "No message found")]))))
