#/bin/bash
#mkdir -p /root/certs
#cd /root/certs

#$1 is droplet ip
#$2 is the droplet host

export PASSWD=$(uuidgen)
echo $PASSWD > ca-key.pem.pass
openssl genrsa -aes256 -passout pass:$PASSWD -out ca-key.pem 4096
openssl req -passin pass:$PASSWD -subj "/C=CA/ST=Ontario/L=Ottawa/O=JRahme/OU=demo/CN=qlik.jrahme.ca" -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem
openssl genrsa -out server-key.pem
openssl req -subj "/CN=$1" -sha256 -new -key server-key.pem -out server.csr
echo subjectAltName = DNS:qlik-demo.jrahme.ca,IP:$1,IP:127.0.0.1 >> extfile.cnf
echo extededKeyUsage = serverAuth >> extfile.cnf
openssl x509 -passin pass:$PASSWD -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem -extfile extfile.cnf
openssl genrsa -out key.pem 4096
openssl req -subj '/CN=client' -new -key key.pem -out client.csr
echo extendedKeyUsage = clientAuth > extfile-client.cnf
openssl x509 -req -passin pass:$PASSWD -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem
rm -v client.csr server.csr extfile.cnf extfile-client.cnf
chmod -v 0400 ca-key.pem key.pem server-key.pem
