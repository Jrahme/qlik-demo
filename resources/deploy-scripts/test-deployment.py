import requests
import os
import json

test_str = os.environ["TEST_STR"]
base_url = "http://{}:8080".format(os.environ["DROPLET_IP"])
tests = {"msg_create" : None,
         "msg_retrieve" : None,
         "msg_update" : None,
         "msg_delete" : None,
         "msg_search" : None,
         "msg_list" : None}


msg_create_result = requests.post("{}/message".format(base_url), data={"msg" : test_str})
tests["msg_create"] = (msg_create_result.status_code in [200, "200"]) 
msg_id = msg_create_result.json()["data"]["created"]["id"]

msg_retreival_result = requests.get("{}/message?id={}".format(base_url, msg_id))
tests["msg_retrieve"] = (msg_retreival_result.json()["msg"] == test_str)

msg_update_result = requests.put("{}/message?id={}".format(base_url, msg_id), data={"msg" : "updated"})
tests["msg_update"] = (msg_update_result.json()["data"]["retrieved"] == test_str) and (msg_update_result.json()["data"]["modified"] == "updated")

msg_search_result = requests.get("{}/search".format(base_url), data={"msg" : "updated"})
tests["msg_search"] = (msg_search_result.status_code in [200, "200"])

msg_list_result = requests.get("{}/messages".format(base_url))
tests["msg_list"] = (msg_list_result.status_code in [200, "200"]) 

msg_delete_result = requests.delete("{}/message?id={}".format(base_url, msg_id))
tests["msg_delete"] = (msg_delete_result.status_code in [200, "200"])

with open("test-results.txt", 'w') as test_results_file:
    test_results_file.write(json.dumps(tests))

if not all(tests):
    raise Exception("Post Deployment tests failed")



