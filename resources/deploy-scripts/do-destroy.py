import requests
import os
import json

api_key = os.environ["DO_API_KEY"]

headers = {"Content-Type": "application/json",
          "Authorization": "Bearer {0}".format(api_key)}

with open("droplet_data.txt", "r") as droplet_info:
    do = droplet_info.read() 

droplet_id = json.loads(do).get("droplet").get("id")
resp = requests.delete("https://api.digitalocean.com/v2/droplets/{}".format(droplet_id), headers=headers)
if not resp.status_code == 204:
    raise Exception("Droplete deletion failed")
else:
    print("Droplet {} Destroyed".format(droplet_id))
