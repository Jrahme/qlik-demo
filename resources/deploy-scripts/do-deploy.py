import requests
import os
import json
from time import sleep
import pprint

pp = pprint.PrettyPrinter()

api_key = os.environ["DO_API_KEY"]

headers = {"Content-Type": "application/json",
          "Authorization": "Bearer {0}".format(api_key)}

with open("droplet_data.txt", 'r') as droplet_file:
    droplet = json.loads(droplet_file.read())["droplet"]
    droplet_id = droplet["id"]

print("Droplet Created, id : {}".format(droplet_id))
pp.pprint(droplet)

# wait till droplet is active
url = "https://api.digitalocean.com/v2/droplets/{}".format(droplet_id)

attempts = 0
while attempts < 3:
    attempts += 1
    droplet_status = requests.get(url, headers=headers)
    if droplet_status.json()["droplet"]["status"] == "active" and droplet_status.status_code == 200:
        print("Droplet {} is now active; took {} attempts".format(droplet_id, attempts))
        break;
    if attempts == 3:
        print("Droplet {} did not become active after one minute. Deployment failed".format(droplet_id))
        raise Exception("Droplet {} Failed to Start".format(droplet_id))
    else:
        print("Droplet not active; waiting 20 seconds; attempt {}".format(attempts))
        sleep(20)

#get the ip
droplet_resp = requests.get("https://api.digitalocean.com/v2/droplets/{}".format(droplet_id), headers=headers)

if droplet_resp.status_code == 200:
    droplet = droplet_resp.json()["droplet"]
    pp.pprint(droplet["networks"]["v4"])
    droplet_ip = droplet["networks"]["v4"][0]["ip_address"]

    with open("droplet_ip.txt", 'w') as droplet_ip_file:
        droplet_ip_file.write(droplet_ip)
