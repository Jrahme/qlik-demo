(ns qlik-demo.test.impl.palindrome
  (:require [clojure.test :refer [is deftest are use-fixtures join-fixtures]]
            [integrant.core :as ig]
            [ring.mock.request :as mock]
            [qlik-demo.impl.palindrome :refer [is-palindrome?]]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen] ))

(s/def ::palindrome
  (s/with-gen
    (s/and string? #(= (clojure.string/reverse %) %))
    #(gen/fmap (fn [s] (apply str s (rest (reverse s))))
               (s/gen string?))))

(s/def ::not-palindrome
  (s/with-gen 
    (s/and string? #(not (= (clojure.string/reverse %) %)))
    #(s/gen string?)))

;; walk a tree to get all paths
(defn paths [m]
  (letfn [(paths* [ps ks m]
            (reduce-kv
              (fn [ps k v]
                (if (map? v)
                  (paths* ps (conj ks k) v)
                  (conj ps (conj ks k))))
              ps
              m))]
    (paths* () [] m)))

(defn check-for-palindromes
  ;; Check if palidrome is valid, v-fn should be either true? or false? depending on if testing for positives or negatives
  [string v-fn]
  (println (str "Verifying \"" string "\" is " (when (= v-fn false?) "not ") "a palindrome"))
  (let [p (is-palindrome? string)]
    (is v-fn (some #{true} (map #(get-in p %1) (paths p)))))
  )

(deftest valid-palindromes
  ;;;generate 25 random palindromes and verify them as palindromes
  (loop [strings (gen/sample (s/gen ::palindrome) 25)]
    (check-for-palindromes (first strings) true?)
    (when (not (empty? (rest strings)))
      (recur (rest strings)))))

(deftest invalid-palidromes
  (loop [strings (gen/sample (s/gen ::not-palindrome) 25)]
    (check-for-palindromes (first strings) false?)
    (when (not (empty? (rest strings)))
      (recur (rest strings)))))




