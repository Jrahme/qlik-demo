FROM openjdk:11.0-jdk-stretch
RUN wget https://raw.githubusercontent.com/technomancy/leiningen/2.8.3/bin/lein && chmod +x lein && mv lein /usr/bin/lein
RUN mkdir -p /source /app
COPY . /src/
RUN cd /src; lein duct setup; lein uberjar; cp target/*-standalone.jar /app/qlik-demo.jar
WORKDIR /app
EXPOSE 3000/tcp
ENTRYPOINT ["java", "-jar", "/app/qlik-demo.jar"]
